#include <stdio.h>
void call_by_val(int,int);
void call_by_ref(int *,int*);
int main()
{
   int a=1,b=2,c=3,d=4;
   printf("\n In main a=%d,b=%d",a,b);
   call_by_val(a,b)
   printf("\n In main a=%d,b=%d",a,b);
   printf("\n In main c=%d,d=%d",c,d);
   call_by_ref(&c,&d)
   printf("\n In main c=%d,d=%d",c,d);
   return 0;
}
void call_by_val(int a,int b)
{
   int temp;
   temp=a;
   a=b;
   b=temp;
   printf("\n In function a=%d,b=%d",a,b);
}
void call_by_ref(int *c,int *d)
{
   int temp;
   temp=*c;
   *c=*d;
   *d=temp;
   printf("\n In function c=%d,d=%d",*c,*d);
}