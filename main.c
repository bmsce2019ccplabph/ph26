#include <stdio.h>
#include <math.h>
int main()
{
  int x1,x2,y1,y2;
  float distance;
  printf("Enter the coordinates of first point in terms of x and y \n");
  scanf("%d %d",&x1,&y1);
  printf("Enter the coordinates of second point in terms of x and y \n");
  scanf("%d %d",&x2,&y2);
  distance=(float)sqrt(pow((x1-x2),2)+pow((y1-y2),2));
  printf("Result=%f",distance);
	return 0;
}
